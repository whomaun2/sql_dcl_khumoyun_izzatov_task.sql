--Create a new user with the username "rentaluser" and the password "rentalpassword". Give the user the ability to connect to the database but no other permissions.
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

--Grant "rentaluser" SELECT permission for the "customer" table. Сheck to make sure this permission works correctly—write a SQL query to select all customers.
GRANT SELECT ON TABLE customer TO rentaluser;
SELECT * FROM customer;

--Create a new user group called "rental" and add "rentaluser" to the group. 
CREATE ROLE rental;
GRANT rental TO rentaluser;

--Grant the "rental" group INSERT and UPDATE permissions for the "rental" table. Insert a new row and update one existing row in the "rental" table under that role. 
GRANT INSERT, UPDATE ON TABLE rental TO rental;
SET ROLE rentaluser
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES ('2023-11-23', 123, 456, '2023-12-01', 1, CURRENT_TIMESTAMP);

UPDATE rental
SET rental_date = '2023-11-27'
WHERE rental_date = '2023-11-23';
RESET ROLE
SELECT * FROM rental WHERE rental_date = '2023-11-27'

--Revoke the "rental" group's INSERT permission for the "rental" table. 
SELECT grantee, privilege_type
FROM information_schema.table_privileges
WHERE table_name = 'rental';
--If rental has UPDATE, you can change to INSERT
REVOKE UPDATE ON TABLE rental FROM rental;
GRANT INSERT ON TABLE rental TO rental;

--Try to insert new rows into the "rental" table make sure this action is denied.
REVOKE INSERT ON TABLE rental FROM rental;
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES ('2023-11-25', 454, 457, '2023-12-05', 1, CURRENT_TIMESTAMP);

--Create a Personalized Role:
CREATE ROLE client_MARY_SMITH;

GRANT SELECT ON TABLE rental TO client_MARY_SMITH;
GRANT SELECT ON TABLE payment TO client_MARY_SMITH;


ALTER TABLE rental DISABLE ROW LEVEL SECURITY;
ALTER TABLE payment DISABLE  ROW LEVEL SECURITY;
CREATE POLICY rental_rolls
    ON rental
    FOR SELECT
    USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'MARY' AND last_name = 'SMITH'));

CREATE POLICY payment_rolls
    ON payment
    FOR SELECT
    USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'MARY' AND last_name = 'SMITH'));


SELECT column_name, data_type
FROM information_schema.columns
WHERE table_name IN ('customer', 'rental', 'payment') AND column_name = 'customer_id';
--Tests:
SET ROLE client_MARY_SMITH;
SELECT * FROM rental;
SELECT * FROM payment;